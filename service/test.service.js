const repository = require('../repository/test.repository')
const ValidationException = require('../exception/validation.exception')
const questionRepo = require('../repository/test.repository')


exports.save = async (test) => {
    if (test == null) {
        throw new ValidationException('É obrigado informar um teste.')
    }

    if (test.name == null || test.name == '') {
        throw new ValidationException('É obrigado informar um nome.')
    }
    await repository.save(test)
}

exports.findAll = async () => {
    return await repository.findAll()
}

exports.questionsConfirmation = async (test) => {
    result = test.forEach(element => {
        const testQuestions = questionRepo.findByTest(element.test._id)
    });
    return result
    
}