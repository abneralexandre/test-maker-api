const repository = require('../repository/question.repository')
const ValidationException = require('../exception/validation.exception')

exports.save = async (question) => {
    if (question == null) {
        throw new ValidationException('É obrigatório informar uma pergunta');
    }

    if (question.description == null || question.description == '') {
        throw new ValidationException("É obrigatório informar uma decrição");
    }

    await repository.save(question)
}

function mapAnswer(answer) {
    return {
        _id: answer._id,
        description: answer.description
    }
}

function mapQuestion(question) {
    return {
        _id: question._id,
        description: question.description,
        answers: question.answers.map(mapAnswer)
    }
}

exports.findByTest = async (test) => {
    let questions = await repository.findByTest(test);
    return questions.map(mapQuestion)
}