const service = require('../service/question.service')

const ValidationException = require('../exception/validation.exception')

module.exports = (app) => {
 
    app.post('/question', async (req, res) => {
        try {
            await service.save(req.body)
            res.end();
        } catch(error) {
            next(error);
        }
    })

    app.get('/question/byTest/:test', async (req, res) => {
        let test = req.params.test

        if(test == null || test == '') {
            throw new ValidationException('É obrigatório informar o test.')
        }

        let questions = await service.findByTest(test)
        res.json(questions)
    })

}