const service = require('../service/test.service')

module.exports = (app) => {

    app.get('/test', async (req, res) => {
        let result = await service.findAll()
        res.json(result)
    })

    app.post('/test', async (req, res) => {
        try {
            await service.save(req.body)
            res.end();
        } catch(error) {
            next.error();
        }
    })
    
    app.post('/test/grade', async (req, res) => {
        try {
            let result = await service.questionsConfirmation(req.body)
            res.json(result);
            res.end();
        } catch(error) {
            next.error();
        }
    })

}